//
//  JSONResultParsingTests.swift
//  topmindKit
//
//  Created by Martin Gratzer on 29/03/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import XCTest
@testable import CoreMind

final class JSONResultParsingTests: XCTestCase {

    func testResultTypeJsonParsing() {
        let result: Result<JSONObject> = .success([:])
        let parsed: Result<ParseOk> = result.parse()

        if case .failure(let error) = parsed {
            XCTFail("\(error)")
        }
    }

    func testResultTypeListJsonParsing() {
        let result: Result<JSONObject> = .success([:])
        let parsed: Result<ParseOk> = result.parse()

        if case .failure(let error) = parsed {
            XCTFail("\(error)")
        }
    }

    func testResultTypeJsonParsingThrows() {
        let result: Result<JSONObject> = .success(["hello": ["world": "1"]])
        let parsed: Result<[ParseNok]> = result.parse(key: "hello")

        if case .success(_) = parsed {
            XCTFail("Should not succeed")
        }
    }

    func testResultTypeListJsonParsingThrows() {
        let result: Result<JSONObject> = .success(["hello": ["world": "1"]])
        let parsed: Result<[ParseNok]> = result.parse(key: "hello")

        if case .success(_) = parsed {
            XCTFail("Should not succeed")
        }
    }
    
}
