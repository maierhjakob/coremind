//
//  Signal.swift
//  topmindKit
//
//  Created by Martin Gratzer on 04/06/2017.
//  Copyright © 2017 topmind mobile app solutions. All rights reserved.
//

import Foundation

public final class Signal<T> {

    private var callbacks = [UUID: (Result<T>) -> ()]()
    private var disposables = [Any]()

    public func subscribe(callback: @escaping (Result<T>) -> ()) -> Disposable {
        let token = UUID()
        callbacks[token] = callback
        return Disposable {
            self.callbacks[token] = nil
        }
    }

    fileprivate func append(disposable: Any) {
        disposables.append(disposable)
    }

    fileprivate func send(_ result: Result<T>) {
        callbacks.forEach {
            $0.value(result)
        }
    }
}

extension Signal {

    public static func pipe() -> ((Result<T>) -> (), Signal<T>) {
        let signal = Signal<T>()
        let weakSend: (Result<T>) -> () = {
            [weak signal] in signal?.send($0)
        }
        return (weakSend, signal)
    }

    public func map<U>(_ f: @escaping (T) -> U) -> Signal<U> {
        let (sink, signal) = Signal<U>.pipe()
        let disposable = subscribe {
            sink( $0.map(f) )
        }
        signal.append(disposable: disposable)
        return signal
    }
}
