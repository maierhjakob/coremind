//
//  Result.swift
//  topmindKit
//
//  Created by Martin Gratzer on 23/08/2016.
//  Copyright © 2016 topmind mobile app solutions. All rights reserved.
//

import Foundation

/// Conform String to Error in order to use it for simpler error throwing
extension String: Error { }

/// The Result type either represents a success - which has an associated value
/// representing the successful result - or a falure - whith an associated error.
/// See http://alisoftware.github.io/swift/async/error/2016/02/06/async-errors/
///
/// - success: represents the successful result
/// - failure: represents a failure with an associated error
public enum Result<T> {
    case success(T)
    case failure(Error)
}

// Mark: - Non throwing values
extension Result {
    /// Unwraps a Result without throwing
    ///
    /// - Returns: nil in case of an error, the value otherwise
    public var value: T? {
        guard case .success(let value) = self else {
            return nil
        }
        return value
    }

    /// Returns the error in case of failure, nil otherwise
    public var error: Error? {
        guard case .failure(let error) = self else {
            return nil
        }
        return error
    }
}

// MARK: - Throwing
extension Result {

    /// Constructs a .success if the expression returns a value or a .failure if it throws
    ///
    /// - parameter throwingExpr: throwing value to evaluate
    ///
    /// - returns: .success if the expression returns a value or a .failure if it throws
    public init(_ throwingExpr: () throws -> T) {
        do {
            self = .success(try throwingExpr())
        } catch {
            self = .failure(error)
        }
    }

    /// Unwraps a Result
    ///
    /// - throws: a .failure error
    /// usefull for result creation/transformation
    /// Result { try result.resolve() }
    ///
    /// - returns: the value if it's a .Success or throw the error if it's a .Failure
    public func resolve() throws -> T {
        switch self {

        case .success(let value):
            return value

        case .failure(let error):
            throw error
            
        }
    }
}


// MARK: - Monad
extension Result {

    public func map<U>(_ f: (T) -> U) -> Result<U> {
        switch self {

        case .success(let result):
            return .success(f(result))

        case .failure(let error):
            return .failure(error)

        }
    }

    public func flatMap<U>( _ f: (T) -> Result<U>) -> Result<U> {
        switch self {

        case .success(let result):
            return f(result)

        case .failure(let error):
            return .failure(error)
            
        }
    }
}

// MARK: - Double Null
extension Result {
    
    public init(_ valueOrNil: T?, _ errorOrNil: Error?) {
        if let error = errorOrNil {
            self = .failure(error)
            
            if let value = valueOrNil {
                logError("Result's value\(value) and error(\(error)) is set")
            }
        } else if let value = valueOrNil {
            self = .success(value)
        } else {
            self = .failure("Double NULL")
        }
    }
}
